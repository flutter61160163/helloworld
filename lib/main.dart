import 'dart:html';

import 'package:flutter/material.dart';

main(List<String> args) {
  runApp(Helloworld());
}

class Helloworld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Hello World', home: Text('Hello World'));
  }
}